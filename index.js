import fs from 'fs'

if (process.argv.length !== 4) {
    console.log('Please provide a year as first argument and day as the second.');
}

let day = process.argv[3];
let year = process.argv[2];
let path = `./${year}/day_${day}/index.js`;

if (!fs.existsSync(path)) {
    console.log(`Day ${day} of year ${year} does not exist`);
} else {
    import(path)
}
