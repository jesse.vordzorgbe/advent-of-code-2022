import url from 'url';
import fs from 'fs'
import path from 'path'

export async function loadInput(importMeta) {
    const dirname = url.fileURLToPath(new URL('.', importMeta.url))
    const input = fs.readFileSync(path.resolve(dirname, 'input.txt'), 'utf-8')
    const test = fs.readFileSync(path.resolve(dirname, 'test.txt'), 'utf-8')
    return { input, test }
}