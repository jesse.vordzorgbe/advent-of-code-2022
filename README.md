# Advent of Code 2022
My Advent of Code setup and solutions.

Advent of Code is an Advent calendar of small programming puzzles for a variety of skill sets and skill levels that can be solved in any programming language.

## Usage
> npm start \<year\> \<day\>

**index.js** + **input.txt** + **test.txt** need to be present in the /year/day folder 
