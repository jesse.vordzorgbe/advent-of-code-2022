import { loadInput } from "../../util.js";

const { input, test } = await loadInput(import.meta)

function parseData(data) {
    return data
        .trim()
        .split('\n')
        .map(line => line.split(' '))
}

function partOne(data) {
    const parsed = parseData(data)
    let score = 0
    for (const round of parsed) {
        let opponent = round[0].charCodeAt(0) - 'A'.charCodeAt(0)
        let you = round[1].charCodeAt(0) - 'X'.charCodeAt(0)

        if (opponent - you === -1 || (opponent === 2 && you === 0))
            score += 6 + you + 1
        else if (opponent - you === 0)
            score += 3 + you + 1
        else
            score += you + 1
    }
    return score
}

function partTwo(data) {
    const parsed = parseData(data)
    let score = 0
    for (const round of parsed) {
        let opponent = round[0].charCodeAt(0) - 'A'.charCodeAt(0)
        let result = round[1].charCodeAt(0) - 'X'.charCodeAt(0)

        if (result === 0) {
            let choice = (opponent - 1) % 3
            if (choice < 0)
                choice = 2
            score += choice + 1
        }
        else if (result === 1) {
            score += 3 + opponent + 1
        }
        else if (result === 2) {
            score += 6 + ((opponent + 1) % 3) + 1
        }
    }
    return score
}

function run(data) {
    const answerOne = partOne(data)
    const answerTwo = partTwo(data)
    console.log(answerOne, answerTwo)
}

run(input)