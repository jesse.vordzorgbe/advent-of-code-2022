import { loadInput } from "../../util.js";

const { input, test } = await loadInput(import.meta)

function parseData(data) {
    return data
        .trim()
        .split('\n\n')
        .map(inventory => inventory.split('\n').map(Number))
}

function partOne(data) {
    const parsed = parseData(data)
    return parsed.map(inventory => inventory.reduce((a, b) => a + b, 0)).sort((a, b) => a > b ? -1 : 1)
}

function partTwo(data) {
    const parsed = parseData(data)
    const full = parsed.map(inventory => inventory.reduce((a, b) => a + b, 0)).sort((a, b) => a > b ? -1 : 1)
    return full.slice(0, 3).reduce((a, b) => a + b, 0)
}

function run(data) {
    const answerOne = partOne(data)[0]
    const answerTwo = partTwo(data)
    console.log(answerOne, answerTwo)
}

run(input)