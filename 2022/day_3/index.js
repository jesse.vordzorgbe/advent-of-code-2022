import { loadInput } from "../../util.js";

const { input, test } = await loadInput(import.meta)

function parseData(data) {
    return data
        .trim()
        .split('\n')
}

function partOne(data) {
    const parsed = parseData(data)
    let sum = 0
    for (const line of parsed) {
        const firstHalf = line.slice(0, line.length / 2)
        const secondHalf = line.slice(line.length / 2)
        for (const char of firstHalf) {
            if (secondHalf.includes(char)) {
                if (char === char.toLowerCase())
                    sum += (char.charCodeAt(0) - 'a'.charCodeAt(0)) + 1
                else
                    sum += (char.charCodeAt(0) - 'A'.charCodeAt(0)) + 27
                break
            }
        }
    }
    return sum
}

function partTwo(data) {
    const parsed = parseData(data)
    let sum = 0
    for (let i = 0; i < parsed.length; i += 3) {
        let lineOne = parsed[i]
        let lineTwo = parsed[i + 1]
        let lineThree = parsed[i + 2]

        for (let i = 0; i < lineOne.length; i++) {
            const char = lineOne[i]
            if (lineTwo.includes(char) && lineThree.includes(char)) {
                if (char === char.toLowerCase()) {
                    sum += (char.charCodeAt(0) - 'a'.charCodeAt(0)) + 1
                }
                else {
                    sum += (char.charCodeAt(0) - 'A'.charCodeAt(0)) + 27
                }
                break
            }
        }
    }
    return sum
}

function run(data) {
    const answerOne = partOne(data)
    const answerTwo = partTwo(data)
    console.log(answerOne, answerTwo)
}

run(input)